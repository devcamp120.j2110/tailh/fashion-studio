import React from "react";
// Style
import styled from "styled-components";
// Component
import CoverVideo from "../components/CoverVideo";
import Logo from "../components/Logo";
import NavBar from "../components/NavBar";
// Style Component
const Section = styled.section`
  position: relative;
  min-height: 100vh;
  overflow: hidden;
`;
const Home = () => {
  return (
    <Section>
      <CoverVideo/>
      <Logo/>
      <NavBar/>
    </Section>
  );
};

export default Home;
