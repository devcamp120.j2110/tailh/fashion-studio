import React from "react";
// Style
import styled from "styled-components";
// Framer Motion
import { motion } from "framer-motion";
// IMG
import Logo from "../assets/Svgs/star_white_48dp.svg";

// Components
const Section = styled.section`
  min-height: 100vh;
  height: auto;
  width: 100vw;
  margin: 5rem auto;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  background-color: ${(pros) => pros.theme.body};
  color: ${(props) => props.theme.text};
  margin-top: 0;
  margin-bottom: 0;
  margin-right: 0;
  position: relative;
`;

const LogoContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  img {
    width: 10vw;
    height: auto;
  }
  h2 {
    font-size: ${(props) => props.theme.fontxxl};
    font-family: "Kaushan Script";
  }
`;
const FooterNavBar = styled(motion.footer)`
  width: 80vw;
  ul {
    display: flex;
    justify-content: space-between;
    align-items: center;
    flex-wrap: wrap;
    margin: 2rem;
    margin-top: 4rem;
    padding: 0 1rem;
    border-top: 1px solid ${(props) => props.theme.text};
    border-bottom: 1px solid ${(props) => props.theme.text};
  }
  li {
    padding: 2rem;
    font-size: ${(props) => props.theme.fontlg};
    text-transform: uppercase;
    cursor: pointer;
    transition: all 0.3s ease;
    &:hover {
      transform: scale(1.2);
    }
  }
`;

const FooterBottom = styled.div`
  padding: 0.5rem 0;
  margin: 0 4rem;
  width: 80vw;
  font-size: ${(props) => props.theme.fontlg};
  display: flex;
  justify-content: space-between;
  align-items: center;
  a {
    text-decoration: underline;
  }
`;

// Variants
const footerNavBarVariants = {
    hidden: {
        y:"-400px",
    },
    show:{
        y:0,
        transition: {
            duration: 1.5,
        }
    },
    viewport:{
        once:false
    }
}

const Footer = () => {
  return (
    <Section>
      <LogoContainer>
        <img 
        data-scroll data-scroll-speed="2"
        src={Logo} alt="Hira Studio" />
        <h2 data-scroll data-scroll-speed="-1" >Hira Studio</h2>
      </LogoContainer>
      <FooterNavBar
        variants={footerNavBarVariants}
        initial="hidden"
        whileInView="show"
        viewport="viewport"
      >
        <ul>
          <li>home</li>
          <li>about</li>
          <li>shop</li>
          <li>
            <a href="https://www.google.com" target="_blank" rel="noreferrer">
              look book
            </a>
          </li>
          <li>
            <a href="https://www.google.com" target="_blank" rel="noreferrer">
              reviews
            </a>
          </li>
        </ul>
      </FooterNavBar>
      <FooterBottom>
        <span>&copy; {new Date().getFullYear()}. All Rights Reserved.</span>
        <span>
          Made with &hearts; By
          <a
            href="https://gitlab.com/devcamp120.j2110/tailh"
            target="_blank"
            rel="noreferrer"
          >
            HuuTai
          </a>
        </span>
      </FooterBottom>
    </Section>
  );
};

export default Footer;
