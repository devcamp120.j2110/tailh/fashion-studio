import React from "react";
// Style
import styled from "styled-components";
// Img
import Image1 from "../assets/Images/1.webp";
import Image3 from "../assets/Images/3.webp";
// Style Component
const Section = styled.section`
  position: relative;
  min-height: 100vh;
  width: 80vw;
  display: flex;
  margin: 0 auto;
`;
const Title = styled.h1`
  font-size: ${(props) => props.theme.fontBig};
  font-family: "Kaushan Script";
  font-weight: 300;
  position: absolute;
  top: 1rem;
  left: 5%;
  z-index: 5;
`;
const ContentLeft = styled.div`
  width: 50%;
  font-size: ${(props) => props.theme.fontlg};
  font-weight: 300;
  position: relative;
  z-index: 5;
  margin-top: 20%;
`;
const ContentRight = styled.div`
  width: 50%;
  position: relative;
  img {
    width: 100%;
    height: auto;
  }
  #small-img-2 {
    width: 40%;
    position: absolute;
    left: 80%;
    bottom: 30%;
  }
`;
const About = () => {
  return (
    <Section id="fixed-target">
      <Title data-scroll data-scroll-speed="1" data-scroll-direction="vertical">
        About Us
      </Title>
      <ContentLeft
        data-scroll
        data-scroll-sticky
        data-scroll-target="#fixed-target"
      >
        We're fashion studio based in california. We create unique designs that
        will blow your mind. We also design unique jewellary pieces. Fashion is
        an ART that can not be grasped by everyone.
        <br />
        <br />
        We are very dedicated to making our products. We offer unique and
        creative products to a wide range of people. We have a variety of
        styles, but for most people, all of the options are in the box. We
        specialize in making things that make you happy.
        <br />
        <br />
        We strive to build on our vision. As a fashion label, we do our best to
        create amazing experiences for all people. We are always looking to make
        something that is easy for everyone.
      </ContentLeft>
      <ContentRight>
        <img src={Image1} alt="Image About 1" />
        <img
          data-scroll
          data-scroll-speed="-2"
          src={Image3}
          id="small-img-2"
          alt="Image About 3"
        />
      </ContentRight>
    </Section>
  );
};

export default About;
