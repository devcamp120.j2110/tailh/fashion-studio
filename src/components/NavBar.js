// Hook
import React, { useState } from "react";
// Style
import styled from "styled-components";
// Framer Motion
import { motion } from "framer-motion";
// Styled Components
const NavBarContainer = styled(motion.div)`
  width: 100vw;
  z-index: 6;
  position: absolute;
  top: ${(props) => (props.showNavBar ? "0" : `-${props.theme.navHeight}`)};

  display: flex;
  justify-content: center;
  align-items: center;
  transition: all 0.5s ease;
`;
const MenuItems = styled(motion.ul)`
  position: relative;
  height: ${(props) => props.theme.navHeight};
  background-color: ${(props) => props.theme.body};
  color: ${(props) => props.theme.text};
  display: flex;
  justify-content: space-around;
  align-items: center;
  width: 100%;
  padding: 0 10rem;
`;
const MenuButton = styled.li`
  background-color: ${(props) => `rgba(${props.theme.textRgba},0.7)`};
  color: ${(props) => props.theme.body};
  width: 15rem;
  height: 2.5rem;
  clip-path: polygon(0 0, 100% 0, 80% 100%, 20% 100%);
  position: absolute;
  top: 100%;
  left: 50%;
  transform: translateX(-50%);
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: ${(props) => props.theme.fontmd};
  font-weight: 700;
  text-transform: uppercase;
  cursor: pointer;
`;
const MenuItem = styled(motion.li)`
  text-transform: uppercase;
  color: ${(props) => props.theme.text};
  cursor: pointer;
`;

//Variants
const NavBarVariants = {
  hidden: {
    y: "-100%",
  },
  show: {
    y: 0,
    transition: {
      duration: 2,
      delay: 2,
    },
  },
};
const itemVariants = {
  hover: {
    scale: 1.1,
    y: -5,
  },
  tap: { //Hiệu ứng khi click chuột
    scale: 0.9,
    y: 0,
  }
};

const NavBar = () => {
  // State
  const [showNavBar, setShowNavBar] = useState(false);
  // Handle Event
  const handleShowNavBar = () => {
    setShowNavBar(!showNavBar);
  };
  return (
    <NavBarContainer
      showNavBar={showNavBar}
      variants={NavBarVariants}
      initial="hidden"
      animate="show"
    >
      <MenuItems
        drag="y"
        dragConstraints={{
          top: 0,
          bottom: 80,
        }}
        dragElastic={0.05}
        dragSnapToOrigin
      >
        <MenuButton onClick={handleShowNavBar}>Menu</MenuButton>
        <MenuItem variants={itemVariants} whileHover="hover" whileTap="tap">
          home
        </MenuItem>
        <MenuItem variants={itemVariants} whileHover="hover" whileTap="tap">
          about
        </MenuItem>
        <MenuItem variants={itemVariants} whileHover="hover" whileTap="tap">
          shop
        </MenuItem>
        <MenuItem variants={itemVariants} whileHover="hover" whileTap="tap">
          new arrival
        </MenuItem>
      </MenuItems>
    </NavBarContainer>
  );
};

export default NavBar;
